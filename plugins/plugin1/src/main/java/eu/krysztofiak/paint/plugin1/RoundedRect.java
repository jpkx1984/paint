package eu.krysztofiak.paint.plugin1;

import eu.krysztofiak.paint.common.DisplayContext;
import eu.krysztofiak.paint.common.Shape;
import eu.krysztofiak.paint.common.ShapeContext;
import eu.krysztofiak.paint.common.ShapePlugIn;

import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import java.awt.geom.Point2D;

import java.awt.*;



/**
 * Created by janusz on 15.01.17.
 */

@ShapePlugIn(bundle="tr1")
public class RoundedRect implements eu.krysztofiak.paint.common.Shape {
    public String getName() { return "rrect"; }
    public String getGuid() { return "RXXX"; }

    public Object getGlyph() { return null; };

    public void render(DisplayContext dc, ShapeContext sc, Graphics2D g) {
        final Point2D.Double[] pts = new Point2D.Double[2];
        int i = 0;
        for (Point2D.Double p : sc.getPoints()) {
            pts[i++] = p;
        }

        final double w = pts[1].getX() - pts[0].getX();
        final double h = pts[1].getY() - pts[0].getY();

        final double aw = Math.abs(w);
        final double ah = Math.abs(h);


        final RoundRectangle2D rr = new RoundRectangle2D.Double(
                w >= 0 ? pts[0].getX() : pts[0].getX() + w,
                h >= 0 ? pts[0].getY() : pts[0].getY() + h,
                aw, ah, aw / 10, ah / 10);

        final Area ar = new Area(rr);

        g.draw(rr);
    }

}

