package eu.krysztofiak.paint.common;


/**
 * Created by janusz on 08.01.17.
 */
public interface DisplayContext {
    java.awt.geom.Point2D.Double getViewA();
    //
    int getViewWidth();
    int getViewHeight();
    double getRaster();
    //
    double getDocumentWidth();
    double getDocumentHeight();
    //

    java.awt.geom.Area getViewArea();
    java.awt.Point toViewCoords(java.awt.geom.Point2D.Double p);
    java.awt.geom.Point2D.Double toDocumentCoords(java.awt.Point p);
}
