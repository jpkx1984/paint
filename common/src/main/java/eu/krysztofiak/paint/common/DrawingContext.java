package eu.krysztofiak.paint.common;

/**
 * Created by janusz on 13.01.17.
 */
public interface DrawingContext {
    java.awt.Color getColor();
}
