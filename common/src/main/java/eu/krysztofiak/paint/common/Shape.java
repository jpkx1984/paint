package eu.krysztofiak.paint.common;

import java.awt.*;

/**
 * Created by janusz on 08.01.17.
 */
public interface Shape {
    String getName();
    String getGuid();

    Object getGlyph();

    /**
     * Renderuj kształt
     * @param dc kontekst wyświetlania (rozmiary dokumentu, widoku)
     * @param sc kontekst kształty (punkty, kontekst rysowania - styl linii, kolor)
     * @param g Java2D
     */
    void render(DisplayContext dc, ShapeContext sc, Graphics2D g);
}
