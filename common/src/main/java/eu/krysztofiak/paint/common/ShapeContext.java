package eu.krysztofiak.paint.common;

import java.awt.geom.Point2D;
import java.util.Collection;

/**
 * Created by janusz on 14.01.17.
 */
public interface ShapeContext extends Comparable {
    int getId();
    Collection<Point2D.Double> getPoints();
    DrawingContext getDrawingContext();
}
