(defproject paint "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.2.395"]
                 [seesaw "1.4.5"]
                 [eu.krysztofiak.paint.common/common "1.3-SNAPSHOT"]
                 [com.google.guava/guava "21.0"]
                 [com.taoensso/nippy "2.12.2"]]
  :main ^:skip-aot paint.core
  :target-path "target/%s"
  :resource-paths ["src/paint/res"]
  :profiles {:uberjar {:aot :all}})
