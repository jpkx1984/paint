(ns paint.core
  (:require [seesaw.core :as s]
            [paint.app :as app]
            [paint.gui :as g])
  (:gen-class))


(def ^:private main-frame (atom nil))

(defn- start-main-frame! []
  (s/invoke-later
     (swap! main-frame g/make-main-frame :dispose? false)))

(defn -main [& args]
  (do
    (app/init!)
    (start-main-frame!)))
