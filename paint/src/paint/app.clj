(ns paint.app
  (:import [eu.krysztofiak.paint.common Shape DisplayContext ShapeContext ShapePlugIn DrawingContext]
           [java.awt Toolkit]
           [java.net URL URI URLClassLoader]
           [java.util Collection]
           [java.awt Color Graphics2D Point]
           [java.awt.geom Point2D$Double Rectangle2D$Double Area]
           [java.awt.image BufferedImage]
           [javax.swing JComponent]
           [java.io File]
           [com.google.common.reflect ClassPath])
  (:require [clojure.core.async :as a]
            [paint.shapes :as sh]
            [paint.document :as doc]
            [paint.tr :as tr]
            [paint.utils :as ut]
            [taoensso.nippy :as nippy])
  (:gen-class))


(def ^:private shapes (atom [(sh/line-shape)]))

(defn fetch-shapes []
  @shapes)


(defrecord DrawingContextImpl [^Color color]
  DrawingContext
  (getColor [this]
    color))



(defprotocol AppContext2
  (document [this] [this ch])
  (alter-document [this f])
  (set-document [this d])
  (select-shape [this ^Shape s])
  (current-shape [this ch] [this])
  (current-dc [this ch] [this])
  (set-dc [this ^DrawingContext dc])
  (alter-dc [this f]))


(defrecord AppContext2Impl [document shape dc events]
  AppContext2
  (document [this] (:document this))
  (alter-document [this f]
    (as-> this t
      (update t :document f)
      (assoc t
             :events (vec (concat
                           (:events t)
                           (for [ev (get-in t [:document :events])] [:document ev]))))
      (assoc-in t [:document :events] [])))
  (set-document [this d]
    (assoc this
           :document d
           :events (conj (:events this) [:document])))
  (select-shape [this s]
    (assoc this
           :shape s
           :events (conj (:events this) [:shape])))
  (current-shape [this]
    shape)
  (current-dc [this]
    dc)
  (set-dc [this d]
    (assoc this
           :dc d
           :events (conj (:events this) [:dc])))
  (alter-dc [this f]
    (-> this
        (update :dc f)
        (update :events conj [:dc]))))

(defn make-app-context2
  ([document]
   (map->AppContext2Impl
    {:document document
     :shape (first (fetch-shapes))
     :dc (map->DrawingContextImpl {:color Color/BLACK})
     :events []}))
  ([] (make-app-context2 (doc/make-document2 500.0 500.0))))


(defprotocol Observable
  (current-value [this])
  (dispatch! [this f])
  (listen! [this topic ch] [this topic]))

(defn make-observable [o topic-foo]
  (let [ch (a/chan)
        p (a/pub ch topic-foo)
        v (agent o)]
    (reify Observable
      (current-value [this]
        @v)
      (listen! [this topic chin]
        (let [tc (a/chan)]
          (a/sub p topic tc)
          (a/pipeline 1 chin (map second) tc)
          (a/go (a/>! chin @v))
          chin))
      (listen! [this topic]
        (let [out (a/chan (a/sliding-buffer 20))]
          (listen! this topic out)
          out))
      (dispatch! [this f]
        (send v (fn [t]
                  (let [tv (f t)]
                    (a/go
                      (doseq [ev (:events tv)]
                        (a/>! ch [ev tv])))
                    (assoc tv :events []))))))))

(def ^:dynamic *plugin-dir* "./plugins")


(defn- load-jars! [paths]
  (let [cloader (URLClassLoader. (into-array URL paths))
        cp (ClassPath/from cloader)
        check-name (fn [s]
                     (.startsWith s "eu.krysztofiak"))]
    ;; szukaj klas
    (doseq [ci (.getTopLevelClasses cp) :when (check-name (.getPackageName ci))  :let [cd (.load ci)]]
      (when-let [ann (.getAnnotation cd ShapePlugIn)]
        ;; ładuj zasób
        (tr/load! cloader (.bundle ann))
        (swap! shapes (fn [s] (conj s (.newInstance cd))))
        ;(println (.getName ci))
        ))))

(defn init! []
  (do
    (tr/load!)
    (load-jars! (for [f (.listFiles (File. *plugin-dir*)) :when (and (.isFile f) (.endsWith (.getName f) ".jar"))]
                  (.toURL (.toURI f))))))
;;


(defn new-file-a []
  )


;;
(nippy/extend-freeze Point2D$Double ::point-2d ; A unique (namespaced) type identifier
  [x data-output]
  (.writeDouble data-output (.getX x))
  (.writeDouble data-output (.getY x)))


(nippy/extend-thaw ::point-2d ; Same type id
                   [data-input]
                   (let [x (.readDouble data-input)
                         y (.readDouble data-input)]
                    (Point2D$Double. x y)))

;;
(nippy/extend-freeze Shape ::shape ; A unique (namespaced) type identifier
  [x data-output]
  (.writeUTF data-output (.toString (.getGuid x))))


(nippy/extend-thaw ::shape ; Same type id
                   [data-input]
                   (let [dict (into (hash-map) (for [s (fetch-shapes)] [(.getGuid s) s]))
                         sd (.readUTF data-input)]
                     ;(println (.getGuid (get dict sd)))
                    (get dict sd)))

;;


(defn serialize-doc [d]
  (nippy/freeze d))

(defn deserialize-doc [b]
  (nippy/thaw b))
