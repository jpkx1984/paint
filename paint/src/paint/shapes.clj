(ns paint.shapes
  (:import [eu.krysztofiak.paint.common Shape DisplayContext DrawingContext ShapeContext]
           [java.util Collection]
           [java.awt Color Graphics2D Point]
           [java.awt.geom Point2D Point2D$Double Rectangle2D$Double Area]           
           [javax.swing JComponent])
  (:require [taoensso.nippy :as nippy])
  (:gen-class))


(defrecord ShapeContextImpl
    [^Integer id
     ^DrawingContext dc
     ^Shape shape
     ^Collection points]
  ShapeContext
  (^int getId [this]
   (int id))
  (^int compareTo [this o]
   (int (- id (:id o))))
  (^Collection getPoints [this]
   points)
  (^DrawingContext getDrawingContext [this]
   dc))

(defn line-shape []
  (reify
    Shape
    (getGuid [this] "l1")
    (getName [this] "s_rectangl")
    (getGlyph [this] nil)
    (^void render [this ^DisplayContext dc ^ShapeContext sc ^Graphics2D g]
     (let [[p1 p2] (vec (.getPoints sc))
           [x1 x2] (map (fn [x] (.toViewCoords dc x)) [p1 p2])
           ar (.getViewArea dc)
           tw (- (.getX x2) (.getX x1))
           th (- (.getY x2) (.getY x1))
           r (Rectangle2D$Double. (if (neg? tw) (+ (.getX x1) tw) (.getX x1))
                                  (if (neg? th) (+ (.getY x1) th) (.getY x1))
                                  (java.lang.Math/abs tw)
                                  (java.lang.Math/abs th))
           ra (Area. r)]
       (.setColor g (.getColor (.getDrawingContext sc)))
       (.draw g r)
       (.intersect ar ra)))))

(defn line-shape2 []
  (reify
    Shape
    (getGuid [this] "l2")
    (getName [this] "XXX2")
    (getGlyph [this] nil)
    (^void render [this ^DisplayContext dc ^ShapeContext sc ^Graphics2D g]
     
     )))

