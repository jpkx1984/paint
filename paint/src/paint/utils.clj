(ns paint.utils
  (:require [clojure.core.async :as a]))

(defmacro def-listener [nm source]
  (let [nm* (if (symbol? nm) (name nm) nm)
        listen-foo-symbol (symbol (str "listen-" nm* "-a"))
        listen-ch-symbol (symbol (str nm* "-ch"))
        listen-m-symbol (symbol (str nm* "-m"))        
        listen-foo-def `(defn ~listen-foo-symbol
                          ([ch#]
                           (let [t# (a/tap ~listen-m-symbol ch#)]
                             (a/>!! ch# ~source)     
                             ch#))
                          ([] (~listen-foo-symbol (a/chan (a/sliding-buffer 1)))))
        listen-ch-def `(def ^:private ~listen-ch-symbol (a/chan 1))
        listen-m-def `(def ^:private ~listen-m-symbol (a/mult ~listen-ch-symbol))]
    [listen-ch-def listen-m-def listen-foo-def]))
