(ns paint.document
  (:import [eu.krysztofiak.paint.common Shape DisplayContext ShapeContext]
           [java.awt Toolkit]
           [java.util Collection]
           [java.awt Color Graphics2D Point]
           [java.awt.geom Point2D$Double Rectangle2D$Double Area]
           [java.awt.image BufferedImage]
           [javax.swing JComponent])
  (:require [clojure.core.async :as a]
            [paint.shapes :as sh]
            [taoensso.nippy :as nippy])
  (:gen-class))

(defrecord ViewPort
    [^BufferedImage image
     ^Double raster
     ^Point2D$Double a])

(defn document-shape-context-adder [^ShapeContext sh]
  (fn [m]
    (cond
      (nil? sh) m
      (or (empty? m) (not= 0 (compare sh (peek m)))) (conj m sh)
      :else (conj (pop m) sh))))

(defn ^Point2D$Double to-document-coords [^ViewPort vp ^Point point]
  (let [a (:a vp)
        raster (:raster vp)
        x (+ (.getX a) (* (.getX point) raster))
        y (+ (.getY a) (* (.getY point) raster))]
    (Point2D$Double. x y)))

(defn ^Point to-viewport-coords [^ViewPort vp ^Point2D$Double p]
  (let [a (:a vp)
        ax (.getX a)
        ay (.getY a)
        x (.getX p)
        y (.getY p)
        dx (- x ax)
        dy (- y ay)
        raster (:raster vp)
        tx (/ dx raster)
        ty (/ dy raster)]
    (Point. tx ty)))

(defn ^Area area [^ViewPort vp]
  (let [raster (:raster vp)
        iw (-> vp :image .getWidth)
        ih (-> vp :image .getHeight)]
   (Area. (Rectangle2D$Double. 0 0 (* raster iw) (* raster ih)))))

(defrecord DisplayContextImpl
    [^Double width
     ^Double height
     ^ViewPort view-port]
  DisplayContext
  (getRaster [this] (:raster view-port))
  (getDocumentWidth [this] width)
  (getDocumentHeight [this] height)
  (getViewWidth [this] (.getWidth (:image view-port)))
  (getViewHeight [this] (.getHeight (:image view-port)))
  (getViewA [this] (:a view-port))
  (getViewArea [this] (area view-port))
  (^Point toViewCoords [this ^Point2D$Double p]
    (to-viewport-coords view-port p))
  (^Point2D$Double toDocumentCoords [this ^Point p]
   (to-document-coords view-port p)))




(defmulti serialize (fn [o]))


;;;;;;;

(defprotocol Document2
  "Dokument"
  (^Double width [this] "Szerokość")
  (^Double height [this] "Wysokość")
  (shapes [this])
  (bk-color [this] "Kolor tła")
  (set-bk [this ^Color c])  
  (alter-doc [this foo] "Aplikuje funkcję zmieniającą dokument")
  (render [this ^ViewPort vp]))

(defprotocol History2
  "Historia"
  (save [this] "Zapis stanu")
  (undo [this] "Cofnięcie")
  (redo [this] "Wznowienie")
  (current [this] "Aktualna pozycja")
  (can-undo? [this] "Czy jest gdzie się cofnąć?")
  (can-redo? [this] "Czy można wznowić?")
  (reset-history [this]))



(defrecord Document2Impl [^Double w
                          ^Double h
                          sh
                          history
                          history-pointer
                          history-dirty
                          events
                          bk-col]
  Document2
  (width [this] w)
  (height [this] h)
  (shapes [this] sh)
  (bk-color [this] bk-col)
  (set-bk [this c]
    (assoc this
           :bk-col c
           :events (conj (:events this) :changed-bk)))
  (alter-doc [this foo]
    (assoc this
           :sh (foo (:sh this))
           :history-dirty true
           :events (conj (:events this) :changed)))
  (render [this vp]
    (let [img (:image vp)
          iw (.getWidth img)
          ih (.getHeight img)
          raster (:raster vp)
          ^DisplayContext dc (->DisplayContextImpl
                              w h
                              vp)
          g (.createGraphics img)]
      (.setBackground g bk-col)
      (.clearRect g 0 0 iw ih)
      (.setColor g Color/BLACK)          
      (doseq [s sh]
        (.render (:shape s) dc s g))))  
  History2
  (can-undo? [this]
    (or (> history-pointer 0) (and (>= history-pointer 0) history-dirty)))
  (can-redo? [this]
    (and (< (:history-pointer this) (dec (count (:history this)))) (not (:history-dirty this))))  
  (save [this]
    (clojure.pprint/pprint this)
    (cond-> this
      ;; obetnij końcówkę
      (and (>= history-pointer 0) (<= history-pointer (count history)))
      (assoc :history (vec (take (inc history-pointer) history)))
      ;; ustaw flagi i wskaznik
      true
      (as-> t
          (update t :history conj (:sh t))
        (assoc t
               :events (conj (:events t) :changed-history)
               :history-dirty false
               :history-pointer (dec (count (:history t)))))))
  (undo [this]
    (if-not (can-undo? this) this
            (let [new-pointer (if-not (:history-dirty this) (dec (:history-pointer this)) (:history-pointer this))]
             (assoc this
                    ;; dekrementuj jeśli historia nie jest brudna
                    :history-pointer new-pointer
                    :history-dirty false
                    :sh (get-in this [:history new-pointer])
                    :events (conj (:events this) :changed-history :changed)))))
  (redo [this]
    (if-not (can-redo? this) this
            (let [new-pointer (inc (:history-pointer this))]
             (assoc this
                    :sh (get-in this [:history new-pointer])
                    :history-dirty false
                    :history-pointer new-pointer
                    :events (conj (:events this) :changed-history :changed)))))
  (current [this]
    (if (empty? history) nil (get history history-pointer))))

(defn make-document2 [w h & [shps]]
  (map->Document2Impl
   {:w w
    :h h
    :sh (or shps [])
    :history [(or shps [])]
    :history-pointer 0
    :history-dirty false
    :events [:changed :changed-bk]
    :bk-col Color/GREEN}))


;;

(defn serialize-document [d]
  (nippy/freeze ))

(defn deserialize-document [bts]
  (nippy/thaw bts))
