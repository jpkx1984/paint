(ns paint.gui
  (:require [seesaw.core :as s]
            [seesaw.mig :as sm]
            [seesaw.chooser :as sc]
            [clojure.core.async :as a]
            [clojure.java.io :as io]
            [paint.document :as doc]
            [paint.app :as app]
            [paint.shapes :as sh]
            [paint.tr :as tr])
  (:use [paint.tr :only [tr listen-locale-a switch-locale!]])
  (:import [eu.krysztofiak.paint.common Shape DisplayContext DrawingContext]
           [java.util Locale]
           [java.awt Toolkit]           
           [java.awt.image BufferedImage]
           [java.awt Color Point]
           [java.awt.event WindowEvent]
           [javax.swing JOptionPane]
           [com.google.common.io ByteStreams Files]
           [java.awt.geom Point2D Point2D$Double]
           [java.awt.event MouseEvent])
  (:gen-class))

(def exit-signal (a/chan))

(def app-context2 (app/make-observable
                   (app/make-app-context2)
                   first))

(def main-frame (atom nil))

(defn- make-image-buffer []
  (let [t (Toolkit/getDefaultToolkit)
        d (.getScreenSize t)
        w (.getWidth d)
        h (.getHeight d)]
    (BufferedImage. w h BufferedImage/TYPE_4BYTE_ABGR)))

(defn- open-file! []
  (s/invoke-later
   (when-let [f (sc/choose-file :type :open :multi? false :filters [(sc/file-filter (tr "s_images") (fn [f] (= "pnt" (Files/getFileExtension (.getName f)))))])]
     (a/thread
      (with-open [istr (io/input-stream f)]
        (try
         (let [bs (ByteStreams/toByteArray istr)
               new-doc (app/deserialize-doc bs)]
           (app/dispatch! app-context2 (fn [ac] (app/set-document ac new-doc)) ))
         (catch Exception ex
           ex)))))))

(defn- save-file! []
  (s/invoke-later
    (when-let [f (sc/choose-file :type :save :multi? false :filters [(sc/file-filter (tr "s_images") (fn [f] (= "pnt" (Files/getFileExtension (.getName f)))))])]
      (a/thread
       (with-open [os (io/output-stream f)]
         (try
           (.write os (app/serialize-doc (:document (app/current-value app-context2))))
           (catch Exception ex
             ex)))))))

(defn make-menu []
  (let [locale-ch (listen-locale-a)
        m-new (s/menu-item :listen [:action (fn [& r]
                                              (app/dispatch! app-context2 (fn [ac] (app/set-document ac (doc/make-document2 500 500))))
                                              )])
        m-open (s/menu-item :listen [:action (fn [& r]
                                               (open-file!))])
        m-save (s/menu-item :listen [:action (fn [& r]
                                               (save-file!))])        
        m-exit (s/menu-item :listen [:action (fn [& r]
                                               (a/>!! exit-signal true))])
        m-about (s/menu-item :listen [:action (fn [& r]
                                                (do
                                                  (JOptionPane/showMessageDialog @main-frame
                                                                                 "Paint, Janusz Krysztofiak (69622)"
                                                                                 "O"
                                                                                 JOptionPane/INFORMATION_MESSAGE)
                                                 ;(s/dialog :content "Paint, Janusz Krysztofiak (69622)")
                                                  ))])
        m-help (s/menu :items [m-about])
        m-english (s/menu-item :listen [:action (fn [& r] (switch-locale! :en))])
        m-polish (s/menu-item :listen [:action (fn [& r] (switch-locale! :pl))])
        m-lang (s/menu 
                       :items [m-english m-polish])
        m-file (s/menu 
                       :items [m-new
                               m-open
                               m-save
                               m-exit])
        mb (s/menubar
            :items [m-file
                    m-lang
                    m-help])]
    (a/go-loop []
      (let [locale (a/<! locale-ch)]
        (s/invoke-later
         (s/config! m-new :text (tr "s_new"))
         (s/config! m-open :text (tr "s_open"))
         (s/config! m-save :text (tr "s_save_as"))         
         (s/config! m-exit :text (tr "s_exit"))
         (s/config! m-help :text (tr "s_help"))
         (s/config! m-polish :text (tr "s_polish"))
         (s/config! m-english :text (tr "s_english"))
         (s/config! m-lang :text (tr "s_lang"))
         (s/config! m-about :text (tr "s_about"))                  
         (s/config! m-file :text (tr "s_file"))))
      (recur))
    mb))


(defn make-tools-panel [] (s/toolbar
                           :floatable? false
                           :orientation :horizontal                           
                           :items (for [t (app/fetch-shapes)]
                                    (let [b (s/toggle
                                             :selected? false
                                             :user-data (.getGuid t)
                                             :listen [:action (fn [& r] (app/dispatch! app-context2 (fn [ac] (app/select-shape ac t))))])
                                          bc (app/listen! app-context2 [:shape])
                                          locale-ch (listen-locale-a)]
                                      (a/go-loop []
                                        (s/config! b :selected? (= (s/user-data b) (.getGuid (app/current-shape (a/<! bc)))))
                                        (recur))
                                      (a/go-loop []
                                        (let [locale (a/<! locale-ch)]
                                          (s/invoke-later
                                           (s/config! b :text (tr (.getName t)))))
                                        (recur))
                                      b))))


(defn make-toolbar []
  (let [locale-ch (listen-locale-a)
        hist-ch (app/listen! app-context2 [:document :changed-history])
        btn-open (s/button :listen [:action (fn [& r] (open-file!))])
        btn-save (s/button :listen [:action (fn [& r] (save-file!))])
        btn-undo (s/button :listen [:action (fn [& r] (app/dispatch! app-context2 (fn [ac] (app/alter-document ac doc/undo))))])
        btn-redo (s/button :listen [:action (fn [& r] (app/dispatch! app-context2 (fn [ac] (app/alter-document ac doc/redo))))])]
    (a/go-loop []
      (let [locale (a/<! locale-ch)]
        (s/config! btn-open :text (tr "s_open"))
        (s/config! btn-save :text (tr "s_save_as"))
        (s/config! btn-undo :text (tr "s_undo"))
        (s/config! btn-redo :text (tr "s_redo")))
      (recur))
    (a/go-loop []
      (let [hi (a/<! hist-ch)]
        (when-let [d (app/document hi)]
          (s/config! btn-undo :enabled? (doc/can-undo? d))
          (s/config! btn-redo :enabled? (doc/can-redo? d))))
      (recur))    
    (s/toolbar
     :floatable? false
     :orientation :horizontal
     :items [btn-open btn-save :separator btn-undo btn-redo])))

(defrecord DrawingPanel
    [panel])

(defn make-color-control [color-in-ch color-out-ch]
  (let [current-color (atom Color/BLACK)
        cnv (s/canvas
             :minimum-size [50 :by 50]
             :listen [:mouse-clicked (fn [ev]
                                       (when-let [nc (sc/choose-color :color @current-color)]
                                         (swap! current-color (fn [x] nc))
                                         (a/go (a/>! color-out-ch nc))))]
             :paint (fn [c g]
                      (.setColor g @current-color)
                      (.fillRect g 0 0 (.getWidth c) (.getHeight c))))]
    (a/go-loop []
      (when-let [c (a/<! color-in-ch)]
        (swap! current-color (fn [x] c))
        (s/invoke-later
         (s/repaint! cnv)))
      (recur))
    cnv))

(def id-counter (atom 0))

(defn make-drawing-panel []
  (let [cshape-ch (app/listen! app-context2 [:shape])
        ;;
        cdc-ch (app/listen! app-context2 [:dc])
        ;cdc (atom (app/map->DrawingContextImpl {:color Color/BLACK}))
        ;; kanał odbierający zmiany dokumentu
                                        ;doc-changed-ch (a/chan (a/sliding-buffer 1))
        doc-changed-ch (a/merge
                        [(app/listen! app-context2 [:document])
                         (app/listen! app-context2 [:document :changed])
                         (app/listen! app-context2 [:document :changed-bk])])
        control-ch (a/chan (a/sliding-buffer 100))
        image-buffer (make-image-buffer)
        view-port (doc/map->ViewPort {:image image-buffer
                                      :raster 1.0
                                      :a (Point2D$Double. 0.0 0.0)})
        start-point (ref nil)
        end-point (ref nil)
        dpanel (s/canvas
                :listen [:component-resized (fn [ev]      
                                              )
                         :mouse-pressed (fn [ev]
                                          (dosync
                                        ;(println "Mouse pressed")
                                           (ref-set start-point (.getPoint ev))
                                           (ref-set end-point (.getPoint ev))
                                           (a/>!! control-ch [:start @start-point @end-point])))
                         
                         :mouse-released (fn [ev]                                           
                                           (do
                                        ;(println "Mouse released")
                                             (when (and @start-point @end-point)
                                               (a/>!! control-ch [:dragged @start-point @end-point]))
                                             (dosync
                                              (ref-set start-point nil)
                                              (ref-set end-point nil))))
                         :mouse-dragged (fn [ev]
                                          (do
                                        ;(println "Mouse moving")
                                            (dosync
                                             (when @start-point
                                               (ref-set end-point (.getPoint ev))))
                                            (when (and @start-point @end-point)
                                              (a/>!! control-ch [:dragging @start-point @end-point]))))]
                :paint (fn [c g] (do
                                        ;(println "Painting")
                                   (.drawImage g image-buffer 0 0 nil))))]
    ;; wątek kontrolujący
    (a/thread
      (while true
        (let [last-shape-context (atom nil)
              [evtype p1 p2] (a/<!! control-ch)]
          (let [x1 (doc/to-document-coords view-port p1)
                x2 (doc/to-document-coords view-port p2)]
            (when (and (nil? @last-shape-context) (or (= evtype :start) (= evtype :dragging)))
              ;(println "Dragstart")
              (swap! last-shape-context (fn [ls] (sh/map->ShapeContextImpl {:id @id-counter
                                                                            :dc (app/current-dc (app/current-value app-context2))
                                                                            :shape (app/current-shape (app/current-value app-context2))
                                                                            :points [x1 x2]})))
              (app/dispatch! app-context2 (fn [ac] (app/alter-document ac (fn [d] (doc/alter-doc d (doc/document-shape-context-adder @last-shape-context)))))))
            (when (= evtype :dragging)
              (swap! last-shape-context (fn [x] (assoc x :points [x1 x2])))
              (app/dispatch! app-context2 (fn [ac] (app/alter-document ac (fn [d] (doc/alter-doc d (doc/document-shape-context-adder @last-shape-context))))))
              ;(println "Dragging " (.getX p1) ":" (.getY p1) " to " (.getX p2) ":" (.getY p2))
              ))
          (when (= evtype :dragged)
            (swap! id-counter inc)            
            (swap! last-shape-context (fn [x] nil))
            (app/dispatch! app-context2 (fn [ac] (app/alter-document ac doc/save)))
            ;(println "Dragged " (.getX p1) ":" (.getY p1) " to " (.getX p2) ":" (.getY p2))
            ))))
    ;; wątek rysujący
    (a/thread
      (while true
        ;; render
        ;; 60x na sekundę
        (a/<!! (a/timeout 17))
        ;; czekaj za zmianami dokumentu
        (a/<!! doc-changed-ch)        
        (when-let [d (-> app-context2 app/current-value app/document)]
          ;(println "Rendering")
         (doc/render d view-port))
        ;; wrzucanie w wątku swinga        
        (s/invoke-later
         (s/repaint! dpanel))
        ;(println "Doc changed handling")
        ))
    (->DrawingPanel dpanel)))

(defn make-status-panel [] (s/vertical-panel :items [(s/label "Test")]))

(defn make-attr-panel []
  (let [locale-ch (listen-locale-a)
        color-label (s/label)
        bk-color-label (s/label)
        bk-ch (app/listen! app-context2 [:document :changed-bk])
        dc-ch (app/listen! app-context2 [:dc])
        color-in (a/chan (a/sliding-buffer 1))
        color-out (a/chan (a/sliding-buffer 1))
        color-pick (make-color-control color-in color-out)
        bk-color-in (a/chan (a/sliding-buffer 1))
        bk-color-out (a/chan (a/sliding-buffer 1))
        bk-color-pick (make-color-control bk-color-in bk-color-out)]
    ;; binding
    (a/pipeline 1 color-in (map (fn [x] (.getColor (app/current-dc x)))) dc-ch)
    (a/pipeline 1 bk-color-in (map (fn [x] (doc/bk-color (app/document x)))) bk-ch)
    (a/go-loop []
      (when-let [c (a/<! color-out)]
        (app/dispatch! app-context2 (fn [ac] (app/set-dc ac (app/map->DrawingContextImpl {:color c})))))
      (recur))
    (a/go-loop []
      (when-let [c (a/<! bk-color-out)]
        (app/dispatch! app-context2 (fn [ac] (app/alter-document ac (fn [d] (doc/set-bk d c))))))
      (recur))    
    (a/go-loop []
      (a/<! locale-ch)
      (s/config! color-label :text (tr "s_color"))
      (s/config! bk-color-label :text (tr "s_bk_color"))
      (recur))
    (sm/mig-panel
     :constraints ["" "[center]" "[][][][]"]
     :items [[color-label "spanx, wrap"]
             [color-pick "spanx, wrap"]
             [bk-color-label "spanx, wrap"]
             [bk-color-pick "spanx, wrap"]])))

(defn make-layout []
  (let [dp (make-drawing-panel)]
    (s/border-panel
     :north (sm/mig-panel 
                          :items [[(make-toolbar) "spanx, wrap"] [(make-tools-panel) "spanx"]])
     :west (make-attr-panel)
     :center (:panel dp)
     :south (make-status-panel))))

(defn- make-main-frame* [t & {dispose? :dispose?}]
  (let [f (s/frame :title "Paint",
                   :size [800 :by 600]
                   :menubar (make-menu)
                   :content (make-layout),
                   :on-close (if dispose? :dispose :exit))]
    (swap! main-frame (fn [x] f))
    (a/go
      (when (a/<! exit-signal)
        (s/invoke-later
         (.dispatchEvent f (WindowEvent. f WindowEvent/WINDOW_CLOSING))
         ;myFrame.dispatchEvent(new WindowEvent(myFrame, WindowEvent.WINDOW_CLOSING));
         )))
    (-> f
        ;s/pack!
        s/show!)
    f))

(defn make-main-frame
  ([] (make-main-frame* nil :dispose? false))
  ([& a] (if (even? (count a))
           (apply make-main-frame* (cons nil a))
           (apply make-main-frame* a))))
