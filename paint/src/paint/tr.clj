(ns paint.tr
  (:require [clojure.core.async :as a]
            [paint.utils :as ut])
  (:import
   [java.lang ClassLoader]
   [java.util ResourceBundle Locale]
   [java.net URL URLClassLoader])
  (:gen-class))

(def expected-locales {:en (Locale. "en", "US") :pl (Locale. "pl", "PL")})

(def ^:private current-locale (atom (first (vals expected-locales))))


(ut/def-listener locale @current-locale)

(def ^:private cache (reduce (fn [a v] (assoc a v (atom {}))) {} (vals expected-locales)))

(defn switch-locale*! [^Locale l]
  ;{:pre [(contains? (vec (vals expected-locales)) l)]}
  (do
    ;(println "New locale")
    (swap! current-locale (fn [_] l))
    (Locale/setDefault l)
    (a/>!! locale-ch l)))

(defn switch-locale! [l]
  {:pre [(keyword? l) (get expected-locales l)]}
  (switch-locale*! (get expected-locales l)))


(defn load! [& [cloader props]]
  (let [props (or props "translations")]
   (doseq [l (vals expected-locales)]
     (when-let [bundle (if cloader (ResourceBundle/getBundle props l cloader) (ResourceBundle/getBundle props l))]
       ;(println (vec (.keySet bundle)))
       (swap! (get cache l)
              merge
              (reduce (fn [a v] (assoc a v (.getString bundle v)) ) {} (.keySet bundle)))))))


(defn tr [s]
  (let [s (or s "")]
    ;(println s)
    (if-let [v (get @(get cache @current-locale) s)]
      v
      s)))
